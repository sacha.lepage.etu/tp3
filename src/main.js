import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas

//console.log(document.querySelectorAll('section h4'))[1];

//const logo = document.querySelectorAll('nav a')[0];
//logo.innerHTML += `<small>les pizzas c'est la vie</small>`;

//const url = document.querySelectorAll('footer div a')[1];
//console.log(url);

//const laCarte = document.querySelector('header nav .mainMenu .pizzaListLink');
//laCarte.setAttribute('class', "active");

const newsContainer = document.querySelector('.newsContainer');
const leBouton = document.querySelector('.closeButton');
newsContainer.setAttribute('style', "");
leBouton.addEventListener('click', event => {
    newsContainer.setAttribute('style', "display:none");
})

const aboutPage = new Component('section', null, 'Ce site est génial');
const pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');



