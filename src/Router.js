export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}

	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		const elem = element.querySelectorAll(`a`);
		console.log(elem);
		elem.forEach(element => {
			element.addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(element.getAttribute('href'));
			});
		});
	}
	
}
